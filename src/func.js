const getSum = (str1, str2) => {
  if(/[a-z]/i.test(str1) || /[a-z]/i.test(str2))
    return false;
  if(typeof(str1) !== "string" || typeof(str2) !== "string")
    return false;

  let result = "";

  if(str1.length === 0)
    str1 = "0";
  if(str2.length === 0)
    str2 = "0";

  if(str1.length > str2.length){
    let difference = str1.length - str2.length;
    for(let i = 0; i < difference; i++)
      str2 = "0" + str2;
  }
  if(str1.length < str2.length){
    let difference = str2.length - str1.length;
    for(let i = 0; i < difference; i++)
      str1 = "0" + str1;
  }

  let temp = 0;

  for(let i = str1.length-1; i>=0; i--){
    let addition = parseInt(str1[i]) + parseInt(str2[i]) + temp;

    if(addition >= 10 && str1[i-1] !== undefined){
      result = addition.toString().charAt(1) + result;
      temp = 1;
      continue;
    }
    temp = 0;
    result = addition + result;
  }

  return result;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let quantityOfPosts = 0;
  let quantityOfComments = 0;

  for(let i = 0; i < listOfPosts.length; i++){
    if(listOfPosts[i].author === authorName)
      quantityOfPosts++;
  }

  for(let el of listOfPosts){
    if(el.comments !== undefined){
      for(let i = 0; i < el.comments.length ; i++){
        if(el.comments[i].author === authorName)
          quantityOfComments++;
      }
    }
  }

  return `Post:${quantityOfPosts},comments:${quantityOfComments}`;
};

const tickets=(people)=> {
  let bills = {
    25: 0,
    50: 0
  }

  for(let p of people){
    let val = typeof(p) === "string" ? parseInt(p) : p;
    if(val === 25){
      bills[25]++;
      continue;
    }
    val -= 25;
    if(val > 50){
      if(bills[25] === 0 || (bills[50] === 0 && bills[25] < 3)) return "NO";
      else {
        if(bills[50] === 0) bills[25] -= 3; else {
          bills[50]--;
          bills[25]--;
        }
      }
    }
    else{
      if(bills[25] === 0) return "NO";
      else{
        bills[25]--;
        bills[50]++;
      }
    }
  }
  return "YES";
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
